# !important links for backend developers #

[Json structure you should be using](https://bitbucket.org/mindkraft2k16/mindkrakft-web-repo/wiki/What%20should%20be%20the%20structure%20of%20json%20that%20we%20will%20deal%20with)

[JSON and PHP](https://bitbucket.org/mindkraft2k16/mindkrakft-web-repo/wiki/Processing%20json%20in%20php)

[How to use postman](https://bitbucket.org/mindkraft2k16/mindkrakft-web-repo/wiki/Using%20postman%20to%20test%20backend%20APIs%20without%20front%20end)

[JSON IN JSON  OUT TUTORIAL](https://bitbucket.org/mindkraft2k16/mindkrakft-web-repo/wiki/Json%20in%20Json%20out%20tutorial)

# Setting up local repository #

* Install git client
* Click on the the clone button inside your mindkraft repo online.
* Copy the link.
* Goto Xampp > htdocs
* RightClick > select "Git Bash Here"
* Paste here and enter

Now git will copy the online repository to your xampp > mindkrakft-web=repo

Thats it. Now you have successfully cloned the mindkraft repository to your local server root folder.

# Uploading your work onto the repository #

* Save Your work
* Goto your working directory
* RightClick > Select "Git Bash Here"
* Now entre command.

```
#!bash

git add .

```

* now enter command


```
#!bash

git commit

```

Now your vim editor will open and you will be asked to enter a commit message.
Enter here about what you have done and what are you uploading.

Save and exit ( Esc + :wq and enter)

* Now type 


```
#!bash

git pull origin dev

```

* Now enter


```
#!bash

git push origin dev

```

This will start uploading your files.

To check if your files are uploaded or not, login to bitbucket, goto the mindkraft-web-repo repository and goto commits. Here your commit will be shown along with the commit message.

# How to keep your local repo updated with the online repo #

Everytime you begin to work, open git bash in your working folder and enter


```
#!bash

git pull origin dev

```

This will update your local repository.

### Thats all you need to know about git. Happy coding ! ###